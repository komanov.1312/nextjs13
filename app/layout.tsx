import TheHeader from '@/components/TheHeader';
import './globals.css';
import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import TheFooter from '@/components/TheFooter';

const inter = Inter({ subsets: ['cyrillic'] });

export const metadata: Metadata = {
  title: 'Некст 13',
  description: 'Описание',
  viewport: { width: 'device-width', initialScale: 1 },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang='ru'>
      <body className={inter.className} suppressHydrationWarning={true} >
        <TheHeader />
        {children}
        <TheFooter />
      </body>
    </html>
  );
}
