import { Metadata } from 'next';
import Link from 'next/link';

export const metadata: Metadata = {
  title: 'О нас',
  description: 'Описание о нас',
};

export default function AboutLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div>
      <h1>О нас</h1>
      <ul>
        <li>
          <Link href='/about/contacts'>Контакты</Link>
        </li>
        <li>
          <Link href='/about/team'>Команда</Link>
        </li>
      </ul>
      {children}
    </div>
  );
}
