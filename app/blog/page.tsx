import { Metadata } from 'next';
import Link from 'next/link';
import React from 'react';

async function getData() {
  const response = await fetch('https://jsonplaceholder.typicode.com/posts', {
    next: {
      revalidate: 60,
    },
  });
  return response.json();
}

export const metadata: Metadata = {
  title: 'Блог',
  description: 'Описание блога',
};

type Props = {};

export default async function About(props: Props) {
  const posts = await getData();
  console.log('post', posts);
  return (
    <div>
      <h2>Блог</h2>
      <ul>
        {posts.map((item: any) => {
          return (
            <li key={item.id}>
              <Link href={`/blog/${item.id}`}>{item.title}</Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
