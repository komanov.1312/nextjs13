import Link from 'next/link';
import React from 'react';

type Props = {};

const TheHeader = (props: Props) => {
  return (
    <div>
      <ul>
        <li>
          <Link href='/'>Главная</Link>
        </li>
        <li>
          <Link href='/blog'>Блог</Link>
        </li>
        <li>
          <Link href='/about'>О нас</Link>
        </li>
      </ul>
    </div>
  );
};

export default TheHeader;
